#include "ch32v30x_it.h"

void NMI_Handler_Raw(void) __attribute__((interrupt("machine")));
void NMI_Handler_Raw(void)
{
    while(1) {
        __asm__ volatile("ebreak");
    }
}

void HardFault_Handler_Raw(void) __attribute__((interrupt("machine")));
void HardFault_Handler_Raw(void)
{
    while(1) {
        __asm__ volatile("ebreak");
    }
}
