#include "debug.h"
#include "donut.h"
#include "blink.h"

void _init()
{
    uart_stdio_init(DEBUG_UART1, 115200);
    donut_init();
}

int main(void)
{
    while (1) {
        blink_task();
        donut_frame();
    }
}
