#ifndef GPIO_HPP
#define GPIO_HPP

#include "ch32v30x.h"
#include "ch32v30x_rcc.h"

template <u32 BASE, u32 RCC_MASK>
struct GpioPort {
    enum OutType { PushPull = 0x0, OpenCollector = 0x1 << 2 };
    enum OutSpeed { Speed10MHz = 1, Speed2MHz = 2, Speed50MHz = 3 };

    static constexpr auto rccMask = RCC_MASK;

    static constexpr inline GPIO_TypeDef *ptr() { return reinterpret_cast<GPIO_TypeDef *>(BASE); }

    static inline void enableClock()
    {
        auto tmp = RCC->APB2PCENR;
        tmp |= rccMask;
        RCC->APB2PCENR = tmp;
    }

    static constexpr vu32 *cfg(int p)
    {
        if (p > 15) {
            return &ptr()->CFGHR;
        } else {
            return &ptr()->CFGLR;
        }
    }

    static void configOutput(int pin, OutType type = PushPull, OutSpeed speed = Speed50MHz)
    {
        u32 mode = static_cast<u32>(type) | static_cast<u32>(speed);
        u32 tmpreg = *cfg(pin);
        tmpreg &= ~static_cast<u32>(0xF << (pin << 2));
        tmpreg |= mode << (pin << 2);
        *cfg(pin) = tmpreg;
    }
};

namespace Port
{
using A = GpioPort<GPIOA_BASE, RCC_APB2Periph_GPIOA>;
using B = GpioPort<GPIOB_BASE, RCC_APB2Periph_GPIOB>;
using C = GpioPort<GPIOC_BASE, RCC_APB2Periph_GPIOC>;
using D = GpioPort<GPIOD_BASE, RCC_APB2Periph_GPIOD>;
using E = GpioPort<GPIOE_BASE, RCC_APB2Periph_GPIOE>;
// using F = GpioPort<GPIOF_BASE, RCC_APB2Periph_GPIOF>;
// using G = GpioPort<GPIOG_BASE, RCC_APB2Periph_GPIOG>;
}

template <typename P, int pin>
struct DigitalOut {
    static constexpr u32 pinMask = 1UL << pin;

    static inline void init()
    {
        P::configOutput(pin);
    }

    static inline bool readOut()
    {
        return P::ptr()->OUTDR & pinMask;
    }

    static inline void toggle()
    {
        if (readOut())
            off();
        else
            on();
    }

    static inline void on()
    {
        P::ptr()->BSHR = pinMask;
    }

    static inline void off()
    {
        P::ptr()->BCR = pinMask;
    }
};

#endif /* GPIO_HPP */
