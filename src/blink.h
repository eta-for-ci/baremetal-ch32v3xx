#ifndef BLINK_H
#define BLINK_H

#ifdef __cplusplus
extern "C" {
#endif

void blink_task(void);

#ifdef __cplusplus
}
#endif

#endif /* BLINK_H */
