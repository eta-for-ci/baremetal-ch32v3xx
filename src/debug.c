/********************************** (C) COPYRIGHT  *******************************
 * File Name          : debug.c
 * Author             : mribelotta
 * Version            : V1.0.0
 * Date               : 2022/10/08
 * Description        : This file contains all the functions prototypes for UART
 *                      Printf , Delay functions.
 * Copyright (c) 2021 Nanjing Qinheng Microelectronics Co., Ltd.
 * Copyright (c) 2022 Martin Ribelotta
 * SPDX-License-Identifier: Apache-2.0
 *******************************************************************************/
#include "debug.h"

#include <ch32v30x.h>
#include <ch32v30x_usart.h>
#include <ch32v30x_gpio.h>
#include <ch32v30x_rcc.h>

#include <errno.h>
#include <stdint.h>

typedef struct {
    USART_TypeDef *usart;
    vu32 *uartClkEnabler;
    u32 uartClkEnableMask;
    u32 gpioClkEnableMask;
    GPIO_TypeDef *gpioPort;
    u32 gpioMask;
} DebugConf_t;

static const DebugConf_t debugConf[] = {
    { USART1, &RCC->APB2PCENR, RCC_APB2Periph_USART1, RCC_APB2Periph_GPIOA, GPIOA, GPIO_Pin_9 },
    { USART2, &RCC->APB1PCENR, RCC_APB1Periph_USART2, RCC_APB2Periph_GPIOA, GPIOA, GPIO_Pin_2 },
    { USART3, &RCC->APB1PCENR, RCC_APB1Periph_USART3, RCC_APB2Periph_GPIOB, GPIOB, GPIO_Pin_10 },
};

#define ARRAY_LEN(a) ((int)(sizeof(a) / sizeof(*(a))))

static int currentDebugUsart = 0;

void uart_stdio_init(DebugUart_t usart, unsigned int baudrate)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    USART_InitTypeDef USART_InitStructure;

    if (usart < 0 || usart >= ARRAY_LEN(debugConf))
        usart = 0;

    const DebugConf_t *cfg = &debugConf[usart];

    currentDebugUsart = usart;
    *cfg->uartClkEnabler = *cfg->uartClkEnabler | cfg->uartClkEnableMask;
    RCC->APB2PCENR = RCC->APB2PCENR | cfg->gpioClkEnableMask;

    GPIO_InitStructure.GPIO_Pin = cfg->gpioMask;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_Init(cfg->gpioPort, &GPIO_InitStructure);

    USART_InitStructure.USART_BaudRate = baudrate;
    USART_InitStructure.USART_WordLength = USART_WordLength_8b;
    USART_InitStructure.USART_StopBits = USART_StopBits_1;
    USART_InitStructure.USART_Parity = USART_Parity_No;
    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    USART_InitStructure.USART_Mode = USART_Mode_Tx;

    USART_Init(cfg->usart, &USART_InitStructure);
    USART_Cmd(cfg->usart, ENABLE);
}

int _write(int fd, char *buf, int size)
{
    int i;

    (void)fd;

    const DebugConf_t *cfg = &debugConf[currentDebugUsart];

    for (i = 0; i < size; i++) {
        while (USART_GetFlagStatus(cfg->usart, USART_FLAG_TC) == RESET)
            ;
        USART_SendData(cfg->usart, *buf++);
    }

    return size;
}

void *_sbrk(ptrdiff_t incr)
{
    extern char _end[];
    static char *curbrk = _end;

    if ((curbrk + incr) < _end)
        return NULL;

    curbrk += incr;
    return curbrk - incr;
}

void _close(int fd)
{
    (void)fd;
    errno = ENOSYS;
}

int _fstat(int fd, void *st)
{
    (void)fd;
    (void)st;
    errno = ENOSYS;
    return -1;
}

int _isatty(int fd)
{
    switch (fd) {
    case 0:
    case 1:
    case 2:
        return 1;
    default:
        return 0;
    }
}

int _lseek(int fd, long off, int when)
{
    (void)fd;
    (void)off;
    (void)when;
    errno = ENOSYS;
    return -1;
}

int _read(int fd, void *buffer, size_t n)
{
    (void)fd;
    (void)buffer;
    (void)n;
    errno = ENOSYS;
    return -1;
}

int _getpid(void)
{
    return 1;
}

int _kill(int pid, int sig)
{
    (void)pid;
    (void)sig;
    errno = ENOSYS;
    return -1;
}
