#ifndef __DELAY_H__
#define __DELAY_H__

#include <ch32v30x.h>

#ifndef CPU_FREQ
#define TICKS_PER_SEC(n)  (SystemCoreClock * n)
#else
#define TICKS_PER_SEC(n)  (CPU_FREQ * n)
#endif
#define TICKS_PER_MSEC(n) ((TICKS_PER_SEC(1LL) / 1000LL) * n)

#ifdef __cplusplus
extern "C" {
#endif

static inline void init_cycle(void)
{
#ifndef CPU_FREQ
    SystemCoreClockUpdate();
#endif
    SysTick->SR = SysTick->SR & ~(1 << 0);
    SysTick->CTLR = (1 << 5) | (1 << 0) | (1 << 2);
}

static inline uint64_t read_cycle(void)
{
    vu32 *ptr = (vu32 *)&SysTick->CNT;
    u32 h, l;
    do {
        h = ptr[1];
        l = ptr[0];
    } while (h != ptr[1]);

    return (((u64)h) << 32L) | l;
}

#ifdef __cplusplus
}
#endif

#endif /* __DELAY_H__ */
