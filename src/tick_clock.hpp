#ifndef __TICK_CLOCK_HPP__
#define __TICK_CLOCK_HPP__

#include <chrono>
#include <cstddef>

#ifndef CPU_FREQ
#define CPU_FREQ 144000000
#endif

class tick_clock
{
public:
    typedef uint64_t rep;
    typedef std::ratio<1, CPU_FREQ> period;

    typedef std::chrono::duration<tick_clock::rep, tick_clock::period> duration;
    typedef std::chrono::time_point<tick_clock> time_point;

    static const bool is_ready;

    static time_point now();
};

#endif /* __TICK_CLOCK_HPP__ */
