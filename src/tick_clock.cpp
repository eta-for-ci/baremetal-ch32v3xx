#include "tick_clock.hpp"
#include "delay.h"

const bool tick_clock::is_ready { []() -> bool {
    init_cycle();
    return true;
}() };

tick_clock::time_point tick_clock::now()
{
    return time_point { duration { rep { is_ready ? read_cycle() : 0 } } };
}
