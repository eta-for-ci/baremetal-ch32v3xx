#ifndef __DEBUG_H
#define __DEBUG_H

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
    DEBUG_UART1 = 0,
    DEBUG_UART2 = 1,
    DEBUG_UART3 = 2,
} DebugUart_t;

void uart_stdio_init(DebugUart_t usart, unsigned int baudrate);

#ifdef __cplusplus
}
#endif

#endif
