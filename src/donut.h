#ifndef DONUT_H
#define DONUT_H

#ifdef __cplusplus
extern "C" {
#endif

extern void donut_init(void);
extern void donut_frame(void);

#ifdef __cplusplus
}
#endif

#endif /* DONUT_H */
