#include "blink.h"

#include <chrono>
#include "GPIO.hpp"

#include "tick_clock.hpp"

void blink_task(void)
{
    using Led = DigitalOut<Port::C, 1>;

    static auto nextBlinkTime { []() {
        Port::C::enableClock();
        Led::init();
        return tick_clock::now();
    }() };

    auto current = tick_clock::now();
    if (current >= nextBlinkTime) {
        using namespace std::chrono_literals;
        nextBlinkTime = current + 250ms;
        Led::toggle();
    }
}
